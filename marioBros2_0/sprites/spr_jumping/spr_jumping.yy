{
    "id": "c65bf57e-8a46-474f-b31b-1e3053d0582f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b92c7f1c-68ae-4afe-ad09-2ead3dc373ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c65bf57e-8a46-474f-b31b-1e3053d0582f",
            "compositeImage": {
                "id": "508e115b-2af5-4c6f-8e4b-b04244c20093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92c7f1c-68ae-4afe-ad09-2ead3dc373ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5899b64f-641d-4b23-83b1-4fbb45f285aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92c7f1c-68ae-4afe-ad09-2ead3dc373ae",
                    "LayerId": "09a9b9bb-1c44-4523-878e-3ecdb73cca7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "09a9b9bb-1c44-4523-878e-3ecdb73cca7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c65bf57e-8a46-474f-b31b-1e3053d0582f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}