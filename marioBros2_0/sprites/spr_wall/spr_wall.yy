{
    "id": "fa195e82-88f2-4e75-bfe4-9a5705b0e78f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b593f69-cc56-4211-acf7-adae159b347c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa195e82-88f2-4e75-bfe4-9a5705b0e78f",
            "compositeImage": {
                "id": "445cc56a-68f0-4e93-abfe-054b0a9d86a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b593f69-cc56-4211-acf7-adae159b347c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca3b18f5-a537-43f3-92b8-4e73e9614f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b593f69-cc56-4211-acf7-adae159b347c",
                    "LayerId": "bea1b3b1-dc5a-4bd4-82e4-fb369ee81a82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bea1b3b1-dc5a-4bd4-82e4-fb369ee81a82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa195e82-88f2-4e75-bfe4-9a5705b0e78f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}