{
    "id": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 10,
    "bbox_right": 53,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a288b652-a3fe-446a-8e06-05e236b2fec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
            "compositeImage": {
                "id": "9b88e4f9-42d2-41cb-9e02-5794f81b8af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a288b652-a3fe-446a-8e06-05e236b2fec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2d10818-caac-4eca-b506-a12a0ef845c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a288b652-a3fe-446a-8e06-05e236b2fec7",
                    "LayerId": "730a9176-e669-4b12-94bd-71f801fc853e"
                }
            ]
        },
        {
            "id": "9cee6d3e-72fe-4d5f-883a-c8e2f869e314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
            "compositeImage": {
                "id": "5d43ea0b-704b-486a-ae59-a96ab3ee964b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cee6d3e-72fe-4d5f-883a-c8e2f869e314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d1c566-1820-4881-a54d-80e244286e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cee6d3e-72fe-4d5f-883a-c8e2f869e314",
                    "LayerId": "730a9176-e669-4b12-94bd-71f801fc853e"
                }
            ]
        },
        {
            "id": "d80af1e9-02d0-47c2-8141-7b39e76fa111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
            "compositeImage": {
                "id": "262f7032-9a57-4e96-b666-527e4baea468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d80af1e9-02d0-47c2-8141-7b39e76fa111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "485f7ea4-ae62-400e-abd5-27bb74b7d011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d80af1e9-02d0-47c2-8141-7b39e76fa111",
                    "LayerId": "730a9176-e669-4b12-94bd-71f801fc853e"
                }
            ]
        },
        {
            "id": "402443a2-0071-421e-bc4b-e7e8b2cd5d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
            "compositeImage": {
                "id": "308c231e-0f79-40cc-8a8a-907a98a795f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402443a2-0071-421e-bc4b-e7e8b2cd5d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2799f4a-ccf7-4f2c-bef6-d67c9814cf31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402443a2-0071-421e-bc4b-e7e8b2cd5d2c",
                    "LayerId": "730a9176-e669-4b12-94bd-71f801fc853e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "730a9176-e669-4b12-94bd-71f801fc853e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}