{
    "id": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 21,
    "bbox_right": 39,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d262da4a-aa9f-42c9-89e2-a6fd4dd020e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
            "compositeImage": {
                "id": "4476dbb5-cf29-45a0-bbb6-d2d1886060fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d262da4a-aa9f-42c9-89e2-a6fd4dd020e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1473322e-d254-4d66-9a7c-c4fdd9a8ab1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d262da4a-aa9f-42c9-89e2-a6fd4dd020e3",
                    "LayerId": "8267bff2-44e4-437e-b2b6-de18e4804323"
                }
            ]
        },
        {
            "id": "a888cc7a-2c7b-4153-abd7-8db42cf074b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
            "compositeImage": {
                "id": "2c0df1e1-7286-4c79-ab69-7d13984f57c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a888cc7a-2c7b-4153-abd7-8db42cf074b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08315da1-72fe-4557-b5e8-d0bbe4d06cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a888cc7a-2c7b-4153-abd7-8db42cf074b4",
                    "LayerId": "8267bff2-44e4-437e-b2b6-de18e4804323"
                }
            ]
        },
        {
            "id": "5e0d35e1-5a6e-406f-b164-61c60045ba80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
            "compositeImage": {
                "id": "5100d256-fea2-4392-9e35-c372e00c4706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e0d35e1-5a6e-406f-b164-61c60045ba80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab62de4-a277-4fe0-b2a0-348d3c698e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e0d35e1-5a6e-406f-b164-61c60045ba80",
                    "LayerId": "8267bff2-44e4-437e-b2b6-de18e4804323"
                }
            ]
        },
        {
            "id": "8917b0e2-d9f0-42c3-a49d-8ce51afa1e9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
            "compositeImage": {
                "id": "31d0a4bf-68f7-4e1e-9bc3-accb698dff76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8917b0e2-d9f0-42c3-a49d-8ce51afa1e9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f316e1b-5486-4d82-a99f-44ffe7033bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8917b0e2-d9f0-42c3-a49d-8ce51afa1e9c",
                    "LayerId": "8267bff2-44e4-437e-b2b6-de18e4804323"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8267bff2-44e4-437e-b2b6-de18e4804323",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7ab5180-aa31-4def-b703-0bb268a7acd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}