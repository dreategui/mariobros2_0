{
    "id": "edeb1885-bf41-4796-a982-80a493f1575c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 21,
    "bbox_right": 39,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12181fa3-734a-4e92-8919-3d0717ab5b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edeb1885-bf41-4796-a982-80a493f1575c",
            "compositeImage": {
                "id": "485514c7-e771-4f3b-9012-dd083d05e12f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12181fa3-734a-4e92-8919-3d0717ab5b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a81369ce-67bb-4efe-8594-6360264699a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12181fa3-734a-4e92-8919-3d0717ab5b59",
                    "LayerId": "200c6823-0fd0-44dd-a8d1-3bd4c51be54f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "200c6823-0fd0-44dd-a8d1-3bd4c51be54f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edeb1885-bf41-4796-a982-80a493f1575c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}