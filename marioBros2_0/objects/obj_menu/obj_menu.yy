{
    "id": "696ffac2-0314-4f7e-94e2-107c2f9fedcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu",
    "eventList": [
        {
            "id": "e1e5875f-6106-4e1d-a289-3a817561a022",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "696ffac2-0314-4f7e-94e2-107c2f9fedcc"
        },
        {
            "id": "622ea376-8157-41ec-9ad0-eae6597acf3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "696ffac2-0314-4f7e-94e2-107c2f9fedcc"
        },
        {
            "id": "63e8a26c-58f0-457b-99b2-afa4c94df224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "696ffac2-0314-4f7e-94e2-107c2f9fedcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}