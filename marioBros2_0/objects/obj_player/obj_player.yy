{
    "id": "3db4b500-cfba-4a8c-bba1-18d6148d5a11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "e1957eef-07d3-4b3e-ae94-80cf7025d61b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3db4b500-cfba-4a8c-bba1-18d6148d5a11"
        },
        {
            "id": "8305f3f9-57ae-4866-b6f6-eec7941f7182",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3db4b500-cfba-4a8c-bba1-18d6148d5a11"
        },
        {
            "id": "342edc00-57a4-41a6-a4f0-4b1041a6a0bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "296a888f-2605-4770-a080-0dc3bb83e183",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3db4b500-cfba-4a8c-bba1-18d6148d5a11"
        }
    ],
    "maskSpriteId": "edeb1885-bf41-4796-a982-80a493f1575c",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "edeb1885-bf41-4796-a982-80a493f1575c",
    "visible": true
}