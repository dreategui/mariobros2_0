/// @description Inserte aquí la descripción
// Puede escribir su código en este editor
if (self.vsp>0){
	instance_destroy(other);
	global.points = global.points+100;
	audio_play_sound(snd_dead_enemy,1,0);
}else{
	instance_destroy(self);
	instance_destroy(obj_enemy);
	global.playerLifes--;
	audio_play_sound(snd_dead_player,1,0);
	if (global.playerLifes > 0){
		instance_create_depth(256,576,obj_wall.depth,obj_player);
	}else{
		highscore_add("DIEGO",global.points);
		room_restart();
		room_goto_next();
	}
}