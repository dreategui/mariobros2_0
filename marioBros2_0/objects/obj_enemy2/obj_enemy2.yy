{
    "id": "e702cf97-f42c-40fb-a8c7-b5f1c5be735e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy2",
    "eventList": [
        {
            "id": "6e0ae72b-292c-4394-b5b5-9f6681a096c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e702cf97-f42c-40fb-a8c7-b5f1c5be735e"
        },
        {
            "id": "306b81b4-ffb5-4c02-955c-e1fcdf4828c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e702cf97-f42c-40fb-a8c7-b5f1c5be735e"
        }
    ],
    "maskSpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
    "overriddenProperties": null,
    "parentObjectId": "296a888f-2605-4770-a080-0dc3bb83e183",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d8233e15-cfa7-4015-aa2b-d5be04d2a6c7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9ec787fa-7af5-4134-93e3-1855ad5af452",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "36cf8526-b48f-4e4c-8076-a46aa23eba9c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "ee964702-6225-43b8-a745-21a2d47dbe95",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
    "visible": true
}