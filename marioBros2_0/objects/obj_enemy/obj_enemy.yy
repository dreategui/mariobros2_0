{
    "id": "296a888f-2605-4770-a080-0dc3bb83e183",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "7f94173c-9de8-4112-b73e-4da960f359e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "296a888f-2605-4770-a080-0dc3bb83e183"
        },
        {
            "id": "87264a03-c636-48de-925a-3d2595b67b3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "296a888f-2605-4770-a080-0dc3bb83e183"
        }
    ],
    "maskSpriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6ee462d8-4a94-45e6-aac9-688099062af1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e87ebc72-8f7e-4b56-ad21-3967f0ae2337",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "b8cccd94-7e23-4d37-a33c-e88b27311b72",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "2400c3af-515f-4690-a905-67bc50271be2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0dad7e31-67b4-4d62-a428-83bdd581c9fa",
    "visible": true
}