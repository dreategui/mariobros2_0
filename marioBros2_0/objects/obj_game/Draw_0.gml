/// @description Inserte aquí la descripción
// Puede escribir su código en este editor
if global.pause return;

draw_set_font(fnt_short);
draw_set_color(c_black);
draw_text(30,30, "points:");
draw_text(115,30, global.points);
draw_text(room_width-115,30, global.playerLifes);
if(global.playerLifes!=1){
	draw_text(room_width-95,30, "lifes");
}else{
	draw_text(room_width-95,30, "life");
}
draw_set_font(fnt_short);
draw_set_color(c_black);
draw_text(room_width/2.7,30, "PRESS ESC TO PAUSE");