{
    "id": "2bb90c18-3804-4df9-af52-ffd625a28cd9",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_SMW_short",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Super Mario Bros.",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "80ad17e7-6330-4991-a112-f58a6d76d379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 141,
                "y": 132
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e7a4f6e9-fd64-4b68-8cc8-ec2be755a9a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 158
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "666355da-48c6-460b-bb2b-ddef34ee4e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 132
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "84429a5e-1e03-4319-9499-935a31c4b762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 141,
                "y": 28
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a73c93e5-9864-4514-8ab6-867751fc3676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 56,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4a2b365e-2ed8-4527-b0a2-a206571b10dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 74,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "07933cb0-f92f-44e1-b711-d5d626ec1843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 160,
                "y": 28
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7f924f38-cd23-41ba-84a2-821f927320a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 70,
                "y": 158
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2ef5532f-7e28-4193-aae6-982b4453db40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 218,
                "y": 132
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f22b600f-b401-480a-810e-69b1943bbd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 206,
                "y": 132
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2a97abbd-0ab1-4b8d-912c-568c4ce18e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 110,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b1732ca8-4de8-4e66-a740-e3b77f92a6ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 134,
                "y": 106
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "554d736d-1032-4ad9-b72b-77169d999ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 63,
                "y": 158
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "22b3e2d5-3afb-4395-9221-4331c2c2e75d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 70,
                "y": 106
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ec678fde-bd17-4ae9-9eee-075c3c81bb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 158
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e9aeac92-3d57-49c4-a2ab-cb34ad28599a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 146,
                "y": 80
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8317e39f-3d91-4f97-8376-54e6ea11daba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "40969e4d-8e6a-427d-8b5e-937c71d13c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 132
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8896674b-13b1-4277-ada2-9c6a0b68636e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 38,
                "y": 80
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "698f55a8-9180-4674-b571-9956f67365a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 122,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5c967a1b-116e-4edd-b06b-55f9b436a66f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 182,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4c09cb0c-3673-4997-ad15-6a95157f87ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 53,
                "y": 106
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f83497bf-e21e-4ba1-8362-66418b7105ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 106
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "111a0477-6a86-42c2-a390-8371da2e2840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 103,
                "y": 28
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b118793e-1dc0-4b50-9dd4-291c143f8dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 218,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "347d436f-e3b5-4a2d-b692-986bee47a1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 235,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "94edd14f-d4b8-4d8a-ace6-f56593625210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f4941dd2-4e13-48ea-8038-b5dfa46c14aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 239,
                "y": 132
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "643f5f5b-cc40-4718-a1e1-976c5e5dc547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 132
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "de97e938-5dfc-4f76-9ddf-dd94e57d2edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 200,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3c285dfc-7790-401f-a292-26d372abe576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 132
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "aa500d45-dfeb-4301-9eea-52ffbc91b3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 164,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7633e8e8-7129-4461-a732-34b2718bd907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b3bcd4a1-0b59-47bc-90e4-6c5c1842e0bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "57e2ac20-f90a-484a-9727-597f1a756cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 111,
                "y": 132
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9909fe73-5a72-4c14-9871-715bf7516a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 128,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1c8de07e-7af7-4b33-bae4-3c676f78d950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1dca30c6-3f62-4e47-9733-8d3dbf15ba11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 214,
                "y": 106
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a0121114-a299-43f8-a70d-bbba44eee2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 182,
                "y": 106
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "516826e8-2bcf-45a5-8be4-4597f5a2f85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d628f394-d279-44af-b23f-4bb82e23fd8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "01c98ee4-e2e7-4e35-a5d2-c8902a1eec3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4e5c1c39-7cbd-4bc8-868e-9d2226c60b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 218,
                "y": 54
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e8f022f1-8e0d-4353-bfe5-43e9096975ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 36,
                "y": 106
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3bb052b7-3836-41f6-9efc-8128a6f3c17f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 230,
                "y": 106
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a72f9499-310f-4d5f-b272-016033b1fb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9250ab22-fe96-47d9-a6f4-f19ebe0800eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 234,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e9055879-463a-4cdd-8b2a-caf5f6757fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e812a347-25c7-4134-9670-c17ec08d13ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 132
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1cf13516-c233-4679-bcd5-059c46df3d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 84,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eba50295-c980-4fa7-a2b5-a5656a23b028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 20,
                "y": 54
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "216ed443-a3f8-4ecf-9dd9-02b9734361c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4d27f16f-bf9e-459d-b8db-e00f690787fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 56,
                "y": 54
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6cf2e642-ea1f-4d31-9c15-4fbad73eb266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 96,
                "y": 132
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "45ad9e0c-621c-426b-bde8-1b9ad4dde36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 64,
                "y": 28
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "14bc8c3f-e842-4a70-a406-f6aa6d2bd3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b61144e7-2d1f-45ee-aa68-6b8be5d64ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "af8be563-7772-41f9-9e47-a75925b7aac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "239e81bf-ac2d-48b0-a5a7-481274541c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 128,
                "y": 54
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7793dda3-8fc5-4572-88cc-1c5e8e2f9f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b7217d93-a423-4e5d-b146-b133c5e1671e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 164,
                "y": 54
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cf83cd7b-72de-47fc-9d9f-2d717265254d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b710915c-0e6d-42fa-8a52-e280fccdb41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 126,
                "y": 132
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3d078e60-4475-4a5c-bfb1-bf64e7456da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "343ccfcd-79f3-4e02-a87b-b5cf0e6a4c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 77,
                "y": 158
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "94f09511-e9cc-4f47-bea6-a26c94b0085d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1ad12cf0-2761-45a5-abfb-b0842c02056c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 66,
                "y": 132
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1044463b-1a00-465c-8be3-8993f524844d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 198,
                "y": 28
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4d04b2d6-7173-4d01-8876-ebf733c558f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 150,
                "y": 106
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ea9fe15d-b0fd-4877-8db6-202375db2fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 102,
                "y": 106
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e7f5dcf0-e019-45f1-b806-26a848056f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 86,
                "y": 106
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f28e17a7-e6f7-4b05-b41f-981c8670293b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 236,
                "y": 54
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "468deb37-5fa6-47a7-bd3a-207669b12db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 200,
                "y": 54
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8d217f58-f1cf-4720-a915-ef8980d82c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 230,
                "y": 132
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "127b43c6-9a8c-46c0-9b6f-2281b6287178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 182,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a205eca6-d4ba-4c08-a965-400f1da9afe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 19,
                "y": 106
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "78f073bc-9e5d-4845-be7c-588a0053209d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 166,
                "y": 106
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3827bc27-ec47-4854-a7c9-71bfe86837fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cbde10d0-b5db-45d7-a21a-9ae3008a307f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 146,
                "y": 54
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e8ca88f3-b17d-4581-ab84-2f5cd500aea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 54
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "656c72b3-2ed5-4f83-bffa-fe4dd1b0bbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 118,
                "y": 106
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5ffd5dc7-b374-4e6c-88f3-d6c8854e1338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 179,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1a2b0c80-1a25-49ed-b008-90ff512d7a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 92,
                "y": 54
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cdd3dfb0-03ac-4d7f-81cf-75ebc6557150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 74,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "460b8560-8b5a-47ef-8fdd-da3e7eb2a4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e5a65327-e08c-46dd-bfca-d6d8907aec9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 81,
                "y": 132
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6231584b-e73b-4b1d-a13c-b97dc3002ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 44,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4369520b-5297-4829-a733-b6e90520448d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ff481c71-4511-419f-b4e9-c3339f5cfb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a598f72b-bb50-48ac-9182-3739f5120c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e821eac5-8f92-4db4-8fe7-b8af0a3b546e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 216,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4c8aa892-19ec-4032-a17b-a9a9912a3c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 181,
                "y": 132
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "af740861-0c09-40a2-985d-5667cbac41d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 47,
                "y": 158
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "db161a09-85ce-4778-ad8e-b972ea60c653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 168,
                "y": 132
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bd9b830d-70c1-438e-a56c-00b53ea2b2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 23,
                "y": 28
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}