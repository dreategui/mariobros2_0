{
    "id": "70b28b01-8e92-48b2-a46b-d0cc13dfc400",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_short",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "A-Space Demo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4e6d2e05-85ca-4f38-b56f-3320c758f344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 182,
                "y": 98
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "509e75cc-5c1c-4479-8b53-8ddc80e72a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 248,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "518a7e0a-1482-4414-aba0-9dd9dcfa997b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 142,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7dc43c5e-fc3e-43e3-8058-fac304b573f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 202,
                "y": 26
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cda14d96-17a2-4144-b3a5-6ff4b25ba9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "197908a2-3970-4d7f-a94d-8d526e30521b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9f81e087-7836-4c1b-8064-a253ced3416f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8a826c37-ae96-4fed-a9c7-c9ee08442208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 243,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e7712dd4-2ec5-480f-9de6-0df09dbb04bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 211,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6083d3cc-dd0c-4929-a8aa-8ee491839700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 197,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "01d25f26-e26b-4aaa-9ee6-bb63ae1ef2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b6650f71-ee4f-4b8d-bfa8-d56d73acae13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 15,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5e65d1af-eefd-4292-b29f-c5fa2585e92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 228,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9f9905a7-e855-4cdb-8098-8be79ea0032e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 124,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fbfceb3c-459c-40d2-9893-34e28b219c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 223,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f510c924-b5fb-4ded-9fea-c0b678b743ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 58,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "47bc5546-3f12-4126-adbc-acdda1ed5d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "02f4a2d5-3de5-4689-94bf-7f8983447646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 88,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "eb4f00b6-d2a6-47cb-b574-357771632c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4c77d5c7-0469-4807-9a1d-eb41666dfdfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 206,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4f9d017b-2e3a-4925-ab49-7ab3f7fceb70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3e4b888a-d65c-4af8-a9b5-de81471fe92c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "07145aa7-ce3a-4111-a2ce-21f58559506c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f2f13d64-247e-4cff-8ff1-3694e8a87ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "18fc6834-657f-4151-83b7-5be28ed642ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "702bdc21-f3cf-416f-92de-e32a21d0d818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "41f3171b-19d4-453c-9ab2-6ea2601d53d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 238,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "16be732c-4005-4e33-b0b8-4e715ce2b0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "679ca568-7e53-4628-8ba1-2fee822207f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 78,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "796e12e8-87b0-4313-89e1-9b5c27d4645d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 133,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a6d6b1b7-a017-4106-bfab-0a71f91a4dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 48,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f6d279a8-bb64-4269-89fb-9750a2ff7af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 191,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "59902b77-bce4-4a6a-9528-d11a68cb3017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f709dbb3-65c0-40a9-be37-0833b1547121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "93f2d7d8-fbc7-4246-9a1e-3c61f97ebbbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 186,
                "y": 26
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4a095699-4959-4447-84e9-ba2271934b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d12fd496-76e7-4d97-9f73-428f2d7c0760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 26
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "610c7980-24d8-4980-a641-c5ef692ca2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "915391be-d08a-4a19-a1c7-76e46737a2f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "53891973-0adb-438d-835b-827ed91bba0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 26
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2a4144a8-f383-4759-9a9e-1aeacb4c9372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "783d2bb1-4270-4844-8e9d-b383b4c768e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 233,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e25dce65-23d7-4fab-9454-aa7a331a6d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 106,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "06372353-22e5-4ae3-8bd9-9a5507eb55a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7d946f70-43ab-4095-b0be-0a280c78b7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 191,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c3bbc7d4-f012-4a71-8a9a-271b9e1bf80e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "22fd2782-ad5d-407c-8e1a-a47beb8938be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c3e7af9a-5938-4deb-9675-911a10e115d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "33260479-28a1-4dd7-84c6-bed3d8af712b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7b4bef41-be5b-4b4a-892b-646a957b5419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "172cd9a5-4ac1-40db-91a5-50f42c23cebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "12b06f0c-ab06-4064-8c62-e3539589f766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 176,
                "y": 50
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e51ccc13-0131-4693-936f-435554483835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 74
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a1002726-0648-43be-9e3a-098e8b4731fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cf2a35e4-2c84-4f1a-8984-ddec7dd0da22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bb6bbf0f-c208-4deb-92af-dbfae00e90f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "caaa7f43-9147-42bb-be75-8eb4bce4564d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e170d830-6d2b-4ede-8866-1301323cf4c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 26
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "615018b4-adb2-4769-bd52-8063aa9f143b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 50
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "396388b2-cd70-4cc9-a033-9cb69b8aecd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 217,
                "y": 98
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "af93573b-538f-477a-b824-4be636b5f1a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 98
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d6e3f8b3-f00d-4a91-b660-54ee82d78c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 190,
                "y": 98
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3be95375-1060-403d-8c8b-ceb2ccdbdf0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9c4596fe-0e17-466e-99f7-81ae7fc4fd88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "60bf3713-279a-4f54-99c6-953753194958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 150,
                "y": 98
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "87589ab1-5e16-4f13-8fc5-7287538144ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 154,
                "y": 26
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b2aac813-dd07-427b-934f-757f031d6a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 218,
                "y": 26
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0eeae3d7-2436-4ab3-ab05-e3c846a34c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 163,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1b8f38e6-b6c0-4a74-b9b4-bee2cc17483d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 26
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fc5ceeb5-c7ea-42d5-bed1-bf04cb86d6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 161,
                "y": 50
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1a77eaad-5676-4db5-924f-8bc87e61570c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 174,
                "y": 98
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "67f9bed0-4e84-408e-a332-73605509dcc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "84274472-20df-46a4-85d1-7de165de28bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "774854da-98ca-468f-9c2f-cd22f8456361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 7,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "65293b94-8146-4c2d-a2a1-d3d00343b189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -3,
                "shift": 6,
                "w": 8,
                "x": 68,
                "y": 98
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3881f1b4-08fa-4cff-8616-4301e598fed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e38e7da7-9dc2-4e4c-9239-9008b809c313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 166,
                "y": 98
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "58923f18-c4a3-4651-a4db-b4179482bef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "94528958-6bb3-49bb-8c97-f5c848cea25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d80e5c56-52f3-467b-b285-d5a704003898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7eaca9ef-e4d4-4529-bc8e-5abafe33da89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 138,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1f739261-bba2-490c-80a1-c9fe5a3c002f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eff1e029-e156-4b0e-9828-7aec675d8698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 219,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b80edd3f-f558-456b-8f72-5aaf20d1200a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3d21b723-7ae0-48c7-b899-d508fdfa2c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 115,
                "y": 98
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e08ff786-e63a-432a-80cb-ccfb175f1a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0ca53135-4257-45e3-b98e-fbc529606260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 121,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d92add1f-9188-43f1-815e-b9327ebc0a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0ba910a9-af02-4b7b-ac7b-0c27e93e0f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "91db1aae-a5be-4061-bd42-eb49e1404daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c559b8bb-41fd-4a54-a0bf-2b2133af9857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 107,
                "y": 74
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "50cc21df-7781-4796-91c4-1421b6116532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 204,
                "y": 98
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d2941070-c3b6-413a-942a-be3d996bdf0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "021c1118-a894-4f49-b6e9-29f6a7f03a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 158,
                "y": 98
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "207b7886-e441-4ebf-ae3a-c539ec60370b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 27,
                "y": 98
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}