{
    "id": "1bf596a7-19fa-42bc-ab4e-9c4e80d21b2d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_SMW",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Super Mario Bros.",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2bbd259a-a888-4f8e-bcca-36f0a7ad1c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 59,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 172,
                "y": 368
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "98d57fc9-b3d3-462a-a3eb-237a9f85bab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 59,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 381,
                "y": 368
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a0da3e1d-b483-439b-b3c9-e7e1933ba0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 231,
                "y": 368
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e87b1210-df91-4f6f-a54f-c80ac62c7d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 59,
                "offset": 1,
                "shift": 44,
                "w": 41,
                "x": 456,
                "y": 63
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1b6e16f1-9016-47d4-a803-5036633bf6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 59,
                "offset": -1,
                "shift": 39,
                "w": 39,
                "x": 336,
                "y": 124
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "048a282e-6276-4c2b-8ed8-8ff9baccbcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 254,
                "y": 124
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c6ff5012-fb7c-470a-aca1-3ddb05c12898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 59,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 128,
                "y": 124
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4fe021ad-3b76-4125-85c9-bc25ae3f50fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 59,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 32,
                "y": 429
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c73289d6-1035-406f-acf0-9cdbb24067c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 314,
                "y": 368
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7a35ef56-bb82-4a89-ad9e-9245cfc82722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 59,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 338,
                "y": 368
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b47d8d26-0498-4d75-ac88-e77e9451bb15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 59,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 206,
                "y": 185
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "20f45198-faa8-4618-8cbb-d16620031d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 59,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 2,
                "y": 368
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4bed803c-d0ab-4696-a4ee-f62854072b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 59,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 18,
                "y": 429
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8e4ae404-21dc-4439-9977-300619a5638b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 2,
                "y": 307
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d924ca65-3f68-4715-a265-f109fcebe2c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 59,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 429
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0cb2ec16-05ba-4075-9e47-cc193b0fafd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 295,
                "y": 124
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d4cbadc6-0a20-434d-b4ee-aa34014fa79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 59,
                "offset": 0,
                "shift": 38,
                "w": 36,
                "x": 279,
                "y": 246
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cb99545f-2e04-47e8-b16b-96cc6b68c262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 59,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 287,
                "y": 368
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ec36afdf-ae46-4259-960f-f530dbafaee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 125,
                "y": 185
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c30d63da-15c6-45dc-b894-70a52528cfe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 38,
                "x": 406,
                "y": 185
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e911b159-f1c9-4464-b34d-67dc663dfb2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 59,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 84,
                "y": 185
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bb4b015b-6735-4715-b123-03ea5e8d2063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 317,
                "y": 246
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fc71f9c7-c8d0-4898-a725-db9e0673a207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 59,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 333,
                "y": 307
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "76ef6c45-ed71-4c75-8c4d-eae62ef9be66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 59,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 86,
                "y": 124
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "50376fe9-009e-41fe-b83d-eb95ba24f571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 241,
                "y": 246
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "af8778de-d479-442d-902a-74644cbe97e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 391,
                "y": 246
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e1a22301-63d8-41d6-8221-d767c6d5be30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 59,
                "offset": 6,
                "shift": 21,
                "w": 15,
                "x": 474,
                "y": 368
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a133c657-f9ac-4f70-bbb9-caa670dadd3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 59,
                "offset": 6,
                "shift": 21,
                "w": 15,
                "x": 457,
                "y": 368
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "19b1cefa-fb31-46a9-a70b-8593bbd28ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 59,
                "offset": 0,
                "shift": 36,
                "w": 35,
                "x": 428,
                "y": 246
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ef201216-57b7-4507-9ca5-8048fc37257c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 59,
                "offset": 0,
                "shift": 38,
                "w": 37,
                "x": 202,
                "y": 246
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b5919fb0-1f77-4c3e-88da-456c81216131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 59,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 441,
                "y": 307
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a03952d7-4a67-48a0-917d-346f61d2e5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 59,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 162,
                "y": 246
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "59d5a4b5-0229-4fe9-9484-f858ce67caf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 59,
                "offset": -1,
                "shift": 52,
                "w": 51,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9c2a545a-4a94-48ca-896a-c82c20ae5184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 59,
                "offset": 0,
                "shift": 49,
                "w": 47,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "415261a1-8890-4428-8787-28025c4ca0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 59,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 36,
                "y": 368
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "12cb7d6d-c25e-4505-9707-785632f69d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 59,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 82,
                "y": 246
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6d24b493-219d-4ec5-85e2-501371977615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 113,
                "y": 307
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2ce53b77-60ae-4b73-a5fc-d2b8804267e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 150,
                "y": 307
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d9bb8ba9-8380-49e4-bf45-4efb328c4aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 59,
                "offset": 0,
                "shift": 35,
                "w": 34,
                "x": 297,
                "y": 307
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "da8ed989-47cc-4e48-8b9d-5d6ee404bdd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 59,
                "offset": 1,
                "shift": 41,
                "w": 38,
                "x": 42,
                "y": 246
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f330d1e5-f803-49bd-a588-4cf668ba9be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 59,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 2,
                "y": 124
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c0c476e2-5c47-4420-8292-cf69b3fdf0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 59,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 361,
                "y": 368
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e218980c-1909-42c2-826e-6b8041b3bd9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 418,
                "y": 124
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0ba8107a-e4df-41a1-868f-c7b3548098a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 39,
                "y": 307
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8a8fcfed-50a7-423d-87e4-07777a6d5b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 59,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 261,
                "y": 307
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "525fe08d-3f42-4d58-bce2-c5b0c0893713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 59,
                "offset": -1,
                "shift": 50,
                "w": 49,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "15a63f2c-2ebe-4634-9a8d-eb46bd38432e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 59,
                "offset": -1,
                "shift": 40,
                "w": 40,
                "x": 212,
                "y": 124
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d28ee128-bb69-4f68-b1df-45bee5a36894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 59,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 413,
                "y": 63
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e28ca6c5-e8d2-4f27-a88f-c9888f0940e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 59,
                "offset": -1,
                "shift": 36,
                "w": 35,
                "x": 76,
                "y": 307
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a2cda25b-99b7-40db-bd51-b310061b7ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 59,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 326,
                "y": 63
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b3c4101e-ffef-4403-a473-61ebf178822f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 59,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 286,
                "y": 185
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "14ff8210-c4db-4ce2-8bf8-49e9c870dda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 38,
                "x": 246,
                "y": 185
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "692b7695-0b8e-47df-8003-d2eb3d12093a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 59,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 166,
                "y": 185
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e2def95c-a929-4204-b023-735f0c6cffc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 59,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 138,
                "y": 368
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a5f045d2-2a22-473d-a5f4-16c576816aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 59,
                "offset": 0,
                "shift": 46,
                "w": 44,
                "x": 190,
                "y": 63
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6d132c5b-92f4-4fca-9905-269339516975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 59,
                "offset": 0,
                "shift": 50,
                "w": 48,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3baad54c-71e8-4bb0-8fc3-8c4ba7a201bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 59,
                "offset": 0,
                "shift": 48,
                "w": 46,
                "x": 2,
                "y": 63
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "044d655d-6609-4c5f-ab51-7c3e35680afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 59,
                "offset": 0,
                "shift": 46,
                "w": 44,
                "x": 144,
                "y": 63
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9253f859-3136-48a1-a9e7-9ccbf68eae4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 59,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 2,
                "y": 185
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c175e74c-da5b-4bb1-8536-2e097e137c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 59,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 439,
                "y": 368
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e8cd7b89-6758-431d-aac2-c293c131393f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 43,
                "y": 185
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "11db8f96-bef1-479d-be35-ab61ffb5df61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 59,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 421,
                "y": 368
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3c2de115-efcb-4fdf-86aa-bdfcc802057b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 59,
                "offset": 0,
                "shift": 33,
                "w": 32,
                "x": 70,
                "y": 368
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cd5a6869-6550-4b7b-bb21-ac66af109326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 59,
                "offset": 1,
                "shift": 62,
                "w": 58,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "37701459-e1f1-4ec3-bdc8-c8cafc9c470d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 59,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 46,
                "y": 429
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "03478d15-cdac-4790-83e3-a13008e4dc2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 59,
                "offset": 0,
                "shift": 49,
                "w": 47,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a46e1183-1fac-48bf-b4b5-c6e2a9b03c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 59,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 104,
                "y": 368
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "26dc0933-7b14-4b00-bba0-fb21db0b6b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 59,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 2,
                "y": 246
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cf8b5077-4b7d-45a0-826a-1b52da4c65a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 354,
                "y": 246
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ea71992e-7753-467a-8172-7dd05af3798d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 465,
                "y": 246
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "00d570e9-6de5-4e3c-8ad4-78df57ee292c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 59,
                "offset": 0,
                "shift": 35,
                "w": 34,
                "x": 405,
                "y": 307
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e9011b8d-3e6a-46f9-89b6-e2c6e17484b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 59,
                "offset": 1,
                "shift": 41,
                "w": 38,
                "x": 122,
                "y": 246
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "add6068c-acf1-4f33-a194-d11610f54203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 59,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 170,
                "y": 124
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "14735301-e947-4f2c-9ff9-1889b7e59e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 59,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 401,
                "y": 368
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "41737cc8-3ff4-4e8a-9a6b-a3ce32a2f1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 377,
                "y": 124
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9b14dc0c-b349-4c2e-97bc-2138441b6720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 224,
                "y": 307
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c377fd4d-09f5-4a21-8703-8ddf76502de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 59,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 369,
                "y": 307
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e00b47f2-f088-4a89-8d6b-380143045d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 59,
                "offset": -1,
                "shift": 50,
                "w": 49,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f7f1e7fe-2320-490a-9641-09f77ec18e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 59,
                "offset": -1,
                "shift": 40,
                "w": 40,
                "x": 44,
                "y": 124
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ee3586f2-40b5-44c7-aecd-8d40d5843e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 59,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 370,
                "y": 63
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5dd0ba5d-2c1f-4535-ad88-45752f721b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 59,
                "offset": -1,
                "shift": 36,
                "w": 35,
                "x": 187,
                "y": 307
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0b70539e-e022-428c-8a17-464e43856f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 59,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 282,
                "y": 63
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4b1fc827-dcbd-4265-ac94-87ac167cb681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 59,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 326,
                "y": 185
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e78dae65-8796-4a41-85fc-efa8c47030e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 59,
                "offset": 0,
                "shift": 40,
                "w": 38,
                "x": 366,
                "y": 185
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "deb3837d-7b1a-455a-9c73-b70d60377358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 59,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 446,
                "y": 185
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "306efb3f-094a-441b-b6d1-5253f5904226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 59,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 476,
                "y": 307
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4912f07c-385e-4fd2-a0e3-78d43f7297fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 59,
                "offset": 0,
                "shift": 46,
                "w": 44,
                "x": 98,
                "y": 63
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6269fdeb-2d16-4afb-a3d8-8f6f8cb437bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 59,
                "offset": 0,
                "shift": 50,
                "w": 48,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "939ab339-73b7-4840-9421-6221d764564f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 59,
                "offset": 0,
                "shift": 48,
                "w": 46,
                "x": 50,
                "y": 63
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "77779efa-ca47-4d2c-9c3a-8215fff0a55a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 59,
                "offset": 0,
                "shift": 46,
                "w": 44,
                "x": 236,
                "y": 63
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "77cc24b0-998e-473c-95c7-f4d0348e2f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 59,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 459,
                "y": 124
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "015ceece-af63-4d99-af1b-e30e6210bafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 203,
                "y": 368
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bcb3be4a-3cf1-4262-aa76-e440d2f7b2ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 59,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 491,
                "y": 368
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "aa6162ab-87e0-4fb2-afea-f2a04a846030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 259,
                "y": 368
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "84875c62-11ba-478d-9332-1bb0afb6c1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 59,
                "offset": 0,
                "shift": 48,
                "w": 47,
                "x": 415,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 44,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}