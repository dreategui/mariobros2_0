{
    "id": "de8dd54b-2ee2-4351-a117-0b6052dfa870",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_insts",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "A-Space Demo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dc8d30e8-4924-4bce-abc6-39d9bbf5d570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 198,
                "y": 59
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a9b40dd9-eca7-4f85-a9d3-3426d36cd9cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 18,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5a9b402e-2dcb-453b-a0be-29c0aa31241c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 225,
                "y": 59
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "089f5f2a-9afd-4b31-a48f-464983caabfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 21
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "05b4e913-fd98-4580-8a69-0918713d58b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "875528f8-532f-42cd-8d32-17ef4c8d8e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2d69fe08-7787-4bfc-9dd9-fe6beea4d485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 162,
                "y": 21
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8efd6f47-0ccc-4a4a-b70e-2ef3424ea476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 10,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d3c2bcb7-b8bc-46a2-9307-dafbf8c96d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 243,
                "y": 59
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "47f384d1-cf44-464b-85be-3e66eb5b9e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 231,
                "y": 59
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8a11df37-27c5-4677-b458-e173b26535e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 87,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "90f09a67-0a6a-4f52-ae88-cf9ebef8d701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 68,
                "y": 59
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "be8fbd1b-0b0f-41f8-af4e-8508374ef96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 26,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d0cfc582-ea86-4a3f-80fa-d36475f14728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 152,
                "y": 59
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "249502be-90bf-4607-9742-c934bd895189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 14,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "932b632c-01fd-40fb-b38d-11346b622a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 176,
                "y": 59
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "76368c47-0aab-4112-a15c-b524ecfe7420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f9819750-f637-4821-9623-f8490b52df28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 59
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "07a1ae4d-dfdd-4d9c-a55d-699af37eccf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d8a8e590-5fa7-49e1-be07-c9972d728e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e4ff14cb-61c3-4cd5-ac2a-4116bab353de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dcb65e0a-cbe1-40d8-953d-64d11f5c41cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "011f1a33-aa41-4851-9351-76f555f3b8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e3848731-e52b-4a8b-99f0-900c03c41347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 123,
                "y": 21
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "20283661-8b44-4466-8046-12c72a50fc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b0420665-ae41-4b37-a060-6b59f15d9af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b09279d9-199f-484d-9ca3-4737940408f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 30,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2e2847aa-14fb-465f-8ecb-002e1878a9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 6,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a1e5686a-bd95-4162-bc79-619a12b7ac9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 168,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c4eaa877-2027-4848-a2d2-72ad638004fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 144,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "cfe5e29e-a1d8-4f63-99b9-dcd1d9d5b4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 136,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e6935859-bc05-4a37-b5a4-667b0cc6c70d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 110,
                "y": 21
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ee4b70c7-79a3-45e9-b98e-603644ffa208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 58,
                "y": 21
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0ffc3120-ecb0-4735-b20b-b73dc741837a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fbb616e1-94f2-467c-b88a-85877080a74b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 84,
                "y": 21
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "41b6c65a-8e3f-44bb-b072-193ffa450a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d83b9397-d9d7-4e6a-b31e-57402ed82c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d90ec041-b507-498c-b59d-7192acb0710d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 40
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5baa841c-2cd7-47cb-b16e-6c657f054e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "194666dc-afcf-4dc6-874a-19fbdfd846dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d4ff89d9-5575-4f2d-bed4-f92774d4af53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fd13aaf1-ea06-4edb-be98-3e77556f696d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 248,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e6288205-1df1-4171-873f-6d4a78c1d388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 112,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "aab009ce-bfa5-4830-846b-2aa4bc0562fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 175,
                "y": 21
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5614f74b-02ae-435d-98bb-7b976088cb93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b89ae2b1-a88f-429c-87ab-66f2ca8f4a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8e2dce61-659c-4474-89e6-f639f3f5f945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8433d21a-8751-46b6-a039-25895b0d2f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9cc28f4b-9f90-4241-9ba1-0edef0357c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0a921a32-1687-4fa4-8035-885c8ce5cc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3ff00224-6b78-465a-807d-596d651ef705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4a7c00fd-c8ce-4993-a93c-523091fc04fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 211,
                "y": 21
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "db6f35ff-405f-4a08-bb5e-8bf108c902fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 21
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9b12a073-5d9c-4019-86f7-514e6fe4734a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "465a17ab-120d-4339-969a-b23915c2b91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7fb603cc-2d20-46a2-8414-00a91e88f2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5cda68e0-a947-4fa7-9495-b99d7171a174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "886dac36-b001-4c51-bc9a-2eb44c45523d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 21
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f75fe2a0-3887-480e-99e2-2ba6e522b666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 97,
                "y": 21
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7e964b57-4390-4453-ac28-e01d22ff0605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 219,
                "y": 59
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ec195484-ecd8-4645-869f-5ac7f7e012f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 104,
                "y": 59
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "40ee191d-32b9-4f9e-907b-5187ec9b70b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 237,
                "y": 59
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6542b486-ab72-41af-a78d-62ebd87b73b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 59
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2562dacc-8443-4df1-8294-0486acfa5de9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 59
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6fb854ec-f804-4cde-875c-c6834fadc222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 191,
                "y": 59
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "41f3b5dd-cafb-4eb4-ba5b-99fb1bea001e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 40
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5600b3bf-f0ea-463f-bbef-6116b1b87276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 40
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "92fca4f8-38d6-4c67-b575-c2afde9eb59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 21
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d77a4e4c-29e3-479f-bf80-5623706db5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 40
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "331bb30b-5cfc-485e-9fb3-217c15b6fa59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 71,
                "y": 21
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "84edcba3-127e-41dd-a165-468ef797e381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 128,
                "y": 59
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f886e27e-0c03-4e88-9696-0b0d70eb7e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a677a05a-e29e-42e4-acf0-986cf408fe42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 40
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "120eae2a-f54e-4bc2-b44c-0e6ce8f5062b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fcf3f240-c417-44b2-8d43-0aa535fef73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -3,
                "shift": 4,
                "w": 6,
                "x": 160,
                "y": 59
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "49ddbf0a-5b1d-4e80-a173-39d862bf4b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b3b3364f-557a-462b-8e13-90e7a977b6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 205,
                "y": 59
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "061ef8dd-9ac3-41a9-9e2c-258c956d507d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "643f7429-bdea-4220-9387-b3f4ee37e899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8119d8bc-e3bd-4f81-be98-63d3603087fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f30d5a06-afcf-4c85-921f-72d7c4b07eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ab42c05f-3ba3-454e-970e-bbd9833792b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d7b49c45-5c45-4569-b329-b06297ada971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 59
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "69cc8834-5c35-4f57-9909-ba38746df976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 149,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c264a81d-0ad6-4d32-9ae3-e57084530f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 120,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3c6d51b6-13af-4cbd-a15a-d45af6eb312a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "72f3ef3c-b085-4c88-aeb7-0f3a99f4def4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "717acc26-48dc-48d7-92d5-92241c2616da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b474b46f-cf91-4061-b54e-184d89dc4c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 187,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9a6f6bd3-cb20-46b0-88d0-206af1ce2e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6124bb4a-505a-41a9-8250-f3b9bce78f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 40
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "194df6da-eb88-49e1-9901-6642698cb15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 184,
                "y": 59
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6446c916-6c17-49e6-829c-beb15423cc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6ccddf63-d824-401c-abde-d69a491e3ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 212,
                "y": 59
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "22747b1b-c6e8-41af-b328-a8e9bf687391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 59
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 11,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}