//react to inputs
move = key_left + key_right;
hsp = move * movespeed;

if (vsp < 10) vsp += grav;

if (place_meeting(x, y+1, obj_wall)){
	if (!grounded && !key_jump){
		hpk_count = 0;//init horizontal count
		jumping = false;
	}else{
		jumping = true;
	}
	
	//check if player grounded
	grounded = !key_jump;
	
	vsp = key_jump * -jumpspeed;
}

//init hsp_jump_applied
if (grounded){
	hsp_jump_applied = 0;
}

//check horizontal counts
if(move!=0 && grounded){
	hkp_count++;
}else if (move == 0 && grounded){
	hkp_count = 0;
}

//check jumping
if (jumping){
	//check if previously we have jump
	if (hsp_jump_applied == 0){
		hsp_jump_applied = sign(move);
	}
	
	//don't jump horizontal
	if (hkp_count < hkp_count_small){
		hsp = 0;
	}else if (hkp_count >= hkp_count_small && hkp_count < hkp_count_big){//small jump
		hsp = hsp_jump_applied * hsp_jump_constant_small;
	}else{//big jump
		hsp = hsp_jump_applied * hsp_jump_constant_big;
	}
}

//horizontal collision
if (place_meeting(x+hsp, y, obj_wall)){
	while (!place_meeting(x+sign(hsp), y, obj_wall)){
		x += sign(hsp);
	}
	hsp = 0;
}
x += hsp;

//verticall collision
if (place_meeting(x, y+vsp, obj_wall)){
	while (!place_meeting(x, y+sign(vsp), obj_wall)){
		y += sign(vsp);
	}
	vsp = 0;
}

y += vsp;
